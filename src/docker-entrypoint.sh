#!/usr/bin/env bash

set -e

case $1 in
  run)  /var/lib/artemis/bin/artemis run;;
  *)  exec "$@";;
esac
